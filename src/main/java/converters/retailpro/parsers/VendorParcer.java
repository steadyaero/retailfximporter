/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters.retailpro.parsers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Iterator;
import com.aeroaddiction.erpsqllibrary.Controllers.VendorController;
import com.aeroaddiction.erpsqllibrary.classes.PriceCalculationMode;
import com.aeroaddiction.erpsqllibrary.classes.Vendor;
import com.aeroaddiction.retailfximporter.RetailFxImporter;

/**
 *
 * @author rjones
 */
public class VendorParcer
{

    private final VendorController vendorsController;
    private final ArrayNode arrayNode;

    public VendorParcer(ArrayNode arrayNode)
    {
        this.arrayNode = arrayNode;
        vendorsController = new VendorController(RetailFxImporter.sqlClass);
    }

    public boolean processJson()
    {
        for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
            ObjectNode objectNode = (ObjectNode) it.next();
            boolean processed = processLine(objectNode);
            if (!processed) {
                return false;
            }
        }

        return true;
    }

    private boolean processLine(ObjectNode objectNode)
    {
        String vendorCode = objectNode.get("Vend Code").textValue();
        String vendorName = objectNode.get("Company").textValue();

        String phoneMainExt = "";
        String accountNumber = "";
        String email = "";
        String phoneFax = "";
        String phoneCell = "";
        String phoneSecondary = "";
        String phoneSecondaryExt = "";

        String contactLast = objectNode.get("Last").textValue();
        if (contactLast.toLowerCase().contains("ext") && contactLast.matches(".*\\d+.*")) {
            phoneMainExt = contactLast.replaceAll("\\D", ""); //remove all non-digits for ext
            contactLast = contactLast.replaceAll("\\d", ""); //remove all digits for name
            contactLast = contactLast.replaceAll("\\W", ""); //remove all non-word chars for name
            contactLast = contactLast.replaceAll("ext", ""); //remove "ext" for name
        } else if (contactLast.toLowerCase().contains("acct") && contactLast.matches(".*\\d+.*")) {
            accountNumber = contactLast.replaceAll("\\W", "");
            accountNumber = accountNumber.replaceAll("acct", "");
            contactLast = "";
        } else if (contactLast.matches(".+@.+\\..+")) {
            email = contactLast;
            contactLast = "";
        }

        String contactFirst = objectNode.get("First").textValue();
        if (contactFirst.toLowerCase().contains("ext") && contactFirst.matches(".*\\d+.*")) {
            phoneMainExt = contactFirst.replaceAll("\\D", ""); //remove all non-digits for ext
            contactFirst = contactFirst.replaceAll("\\d", ""); //remove all digits for name
            contactFirst = contactFirst.replaceAll("\\W", ""); //remove all non-word chars for name
            contactFirst = contactFirst.replaceAll("ext", ""); //remove "ext" for name
        } else if (contactFirst.toLowerCase().contains("acct") && contactFirst.matches(".*\\d+.*")) {
            accountNumber = contactFirst.replaceAll("\\W", "");
            accountNumber = accountNumber.replaceAll("acct", "");
        } else if (contactFirst.matches(".+@.+\\..+")) {
            email = contactFirst;
            contactFirst = "";
        }

        String phoneMain = objectNode.get("Phone 1").textValue();
        if (phoneMain.matches(".*[a-zA-Z]+.*")) {
            String newString = "";
            for (char ch : phoneMain.toLowerCase().toCharArray()) {
                char newChar = getNumberFromLetter(ch);
                newString += String.valueOf(newChar);
            }
            phoneMain = newString;
        }
        phoneMain = phoneMain.replaceAll("\\W", "");

        String phone2 = objectNode.get("Phone 2").textValue();
        if (phone2.contains("fax")) {
            phoneFax = phone2.replaceAll("\\D", "");
        } else if (phone2.contains("cel")) {
            phoneCell = phone2.replaceAll("\\D", "");
        } else {
            phoneSecondary = phone2.replaceAll("\\D", "");
        }

        PriceCalculationMode priceCalculationMode = vendorsController.priceCalculationModeSelectFromId(1); //msrp
        double priceCalculationFactor = 0.8;

        Vendor vendor = vendorsController.createVendor(vendorCode, vendorName, contactLast, contactFirst,
                                                       phoneMain, phoneMainExt, phoneCell, phoneFax, email, accountNumber,
                                                       priceCalculationFactor, priceCalculationMode);

        if (vendor == null) {
            RetailFxImporter.getMainController().appendOutputMessage("Error inserting vendor: " + vendorName);
            return false;
        }

        return true;
    }

    private char getNumberFromLetter(char ch)
    {
        switch (ch) {
            case 'a':
            case 'b':
            case 'c':
                return '2';

            case 'd':
            case 'e':
            case 'f':
                return '3';

            case 'g':
            case 'h':
            case 'i':
                return '4';

            case 'j':
            case 'k':
            case 'l':
                return '5';

            case 'm':
            case 'n':
            case 'o':
                return '6';

            case 'p':
            case 'q':
            case 'r':
            case 's':
                return '7';

            case 't':
            case 'u':
            case 'v':
                return '8';

            case 'w':
            case 'x':
            case 'y':
            case 'z':
                return '9';
        }

        return ch;
    }
}
