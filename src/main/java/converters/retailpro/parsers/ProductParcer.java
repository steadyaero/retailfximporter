/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters.retailpro.parsers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import com.aeroaddiction.erpsqllibrary.Controllers.AvailabilityController;
import com.aeroaddiction.erpsqllibrary.Controllers.BrandController;
import com.aeroaddiction.erpsqllibrary.Controllers.CategoryController;
import com.aeroaddiction.erpsqllibrary.Controllers.InventoryController;
import com.aeroaddiction.erpsqllibrary.Controllers.LocationController;
import com.aeroaddiction.erpsqllibrary.Controllers.PriceController;
import com.aeroaddiction.erpsqllibrary.Controllers.ProductController;
import com.aeroaddiction.erpsqllibrary.Controllers.UomController;
import com.aeroaddiction.erpsqllibrary.Controllers.VendorController;
import com.aeroaddiction.erpsqllibrary.classes.Brand;
import com.aeroaddiction.erpsqllibrary.classes.Category;
import com.aeroaddiction.erpsqllibrary.classes.Company;
import com.aeroaddiction.erpsqllibrary.classes.Inventory;
import com.aeroaddiction.erpsqllibrary.classes.Price;
import com.aeroaddiction.erpsqllibrary.classes.Product;
import com.aeroaddiction.erpsqllibrary.classes.ProductVendor;
import com.aeroaddiction.erpsqllibrary.classes.ProductCompany;
import com.aeroaddiction.erpsqllibrary.classes.Store;
import com.aeroaddiction.erpsqllibrary.classes.Uom;
import com.aeroaddiction.erpsqllibrary.classes.Vendor;
import com.aeroaddiction.retailfximporter.RetailFxImporter;
import com.aeroaddiction.retailfximporter.SqlController;
import com.aeroaddiction.retailfximporter.SqlHandler;

/**
 *
 * @author rjones
 */
public class ProductParcer
{

    private List<String[]> fileContents;

    TreeMap<String, Brand> brandTM;
    TreeMap<String, Vendor> vendorTM;
    TreeMap<String, Uom> uomTM;
    TreeMap<String, Category> categoryTM; //fullcode

    ArrayList<Vendor> vendorList;
    ArrayList<Brand> brandList;
    ArrayList<Uom> uomList;
    ArrayList<Category> categorylist;

    private ProductController productsController;
    private LocationController locationController;
    private PriceController pricesController;
    private InventoryController inventoryController;
    private VendorController vendorsController;
    private BrandController brandsController;
    private CategoryController categoriesController;
    private UomController uomController;
    private final ArrayNode arrayNode;
    private AvailabilityController availabilityController;
    private ArrayList<Company> companyList;
    private ArrayList<Store> storeList;
    private final ArrayList<String> productInsertList;
    private final ArrayList<String> productVendorInsertList;
    private final ArrayList<String> productCompanyInsertList;
    private final ArrayList<String> pricesInsertList;
    private final ArrayList<String> inventoryInsertList;
    private SqlHandler sqlHandler;
    private SqlController sqlController;

    /**
     *
     * @param arrayNode
     */
    public ProductParcer(ArrayNode arrayNode)
    {
        this.arrayNode = arrayNode;
        initControllers();
        getVendors();
        getBrands();
        getCategories();
        getCompanies();
        getStores();
        getUoms();

        productInsertList = new ArrayList<>();
        productVendorInsertList = new ArrayList<>();
        productCompanyInsertList = new ArrayList<>();
        pricesInsertList = new ArrayList<>();
        inventoryInsertList = new ArrayList<>();
    }

    private void initControllers()
    {
        productsController = new ProductController(RetailFxImporter.sqlClass);
        locationController = new LocationController(RetailFxImporter.sqlClass);
        pricesController = new PriceController(RetailFxImporter.sqlClass);
        inventoryController = new InventoryController(RetailFxImporter.sqlClass);
        vendorsController = new VendorController(RetailFxImporter.sqlClass);
        brandsController = new BrandController(RetailFxImporter.sqlClass);
        categoriesController = new CategoryController(RetailFxImporter.sqlClass);
        uomController = new UomController(RetailFxImporter.sqlClass);
        availabilityController = new AvailabilityController(RetailFxImporter.sqlClass);

        sqlController = new SqlController();
    }

    public boolean processJson()
    {
        boolean result = true;
        int i = 0;
        for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
            i++;
            if (i % 1000 == 0) {
                RetailFxImporter.getMainController().appendOutputMessage("processing line: " + i);
            }

            ObjectNode objectNode = (ObjectNode) it.next();
            String itemNumber = objectNode.get("Item #").textValue();
            String fullCategoryCode = objectNode.get("DCS").textValue();
            String vendorCode = objectNode.get("Vend Code").textValue();
            String vendorItemNumber = objectNode.get("Desc 1").textValue();
            String itemName = objectNode.get("Desc 2").textValue();
            String brandName = objectNode.get("Desc 3").textValue();
            String sizeAndUom = objectNode.get("Size").textValue();
            String upc = objectNode.get("UPC").textValue();

            if (fullCategoryCode.equals("") && vendorCode.equals("") && vendorItemNumber.equals("") && itemName.equals("")) {
                continue;
            }

//            String qty = objectNode.get("Qty").textValue();
//            String min = objectNode.get("Min").textValue();
//            String max = objectNode.get("Max").textValue();
            double cost = Double.valueOf(objectNode.get("Cost").textValue());
            double price = Double.valueOf(objectNode.get("Inven P$").textValue());

            Product product = ProductController.createNullProduct();
            ProductVendor productVendor = productsController.createNullProductVendor();
            ProductCompany productCompany = productsController.createNullProductCompany();

            result &= handleItemNumber(product, itemNumber);
            if (!result) {
                return false;
            }
            result &= handleUpc(product, upc);
            if (!result) {
                return false;
            }
            result &= handleItemName_Vendor(product, itemName, vendorCode, vendorItemNumber, sizeAndUom, productVendor, productCompany);
            if (!result) {
                return false;
            }
            result &= handleBrand(product, brandName);
            if (!result) {
                return false;
            }
            result &= handleSizeUom(product, sizeAndUom);
            if (!result) {
                return false;
            }
            result &= handleCategory(product, fullCategoryCode);
            if (!result) {
                return false;
            }

//      productInsertList.add(productsController.productInsertStringForBatch(product));
            boolean insertProduct = productsController.productInsert(product);
            if (!insertProduct) {
                RetailFxImporter.getMainController().appendOutputMessage("Error inserting product: " + product.getName());
                RetailFxImporter.getMainController().appendOutputMessage(productsController.getException().getMessage());
                return false;
            }

            insertProductVendors(productVendor);
            insertProductCompanies(productCompany);
            insertPricing(product, cost, price);
            insertInventory(product);
        }

        result &= batchInsert();
        return result;
    }

    private boolean batchInsert()
    {
        RetailFxImporter.getMainController().appendOutputMessage("Batch Inserting...");

//    boolean productsInsert = productsController.batchInsert(productInsertList, "products");
//    productInsertList.clear();
//    if(!productsInsert) {System.out.println("Error inserting products"); System.exit(1); }
        RetailFxImporter.getMainController().appendOutputMessage("Inserting productVendors");
        boolean productsVendorInsert = sqlController.batchInsert(productVendorInsertList, "productVendor");
        productVendorInsertList.clear();
        if (!productsVendorInsert) {
            RetailFxImporter.getMainController().appendOutputMessage("Error inserting products vendor");
//            RetailFxImporter.getMainController().appendOutputMessage(sqlController.getException().getMessage());
            return false;
        }

        RetailFxImporter.getMainController().appendOutputMessage("Inserting productCompanys");
        boolean productsCompanyInsert = sqlController.batchInsert(productCompanyInsertList, "productCompany");
        productCompanyInsertList.clear();
        if (!productsCompanyInsert) {
            RetailFxImporter.getMainController().appendOutputMessage("Error inserting products company");
            return false;
        }

//        System.out.println("Inserting inventory");
//        boolean inventoryInsert = productsController.batchInsert(inventoryInsertList, "inventory");
//        inventoryInsertList.clear();
//        if (!inventoryInsert) {
//            System.out.println("Error inserting inventory");
//            System.exit(1);
//        }
        RetailFxImporter.getMainController().appendOutputMessage("Inserting prices");
        boolean pricingInsert = sqlController.batchInsert(pricesInsertList, "prices");
        pricesInsertList.clear();
        if (!pricingInsert) {
            RetailFxImporter.getMainController().appendOutputMessage("Error inserting pricing");
            return false;
        }

        return true;
    }

    private boolean handleItemNumber(Product product, String itemNumber)
    {
        if (itemNumber.equals("")) {
            RetailFxImporter.getMainController().appendOutputMessage("Invalid ItemNumber: " + itemNumber);
            return false;
        }
        product.setItemNumber(Integer.valueOf(itemNumber));

        return true;
    }

    private boolean handleUpc(Product product, String upc)
    {
        if (upc.equals("")) {
            upc = StringUtils.leftPad(String.valueOf(product.getItemNumber()), 12, "0");
        }

        product.setUPC(upc);
        return true;
    }

    private boolean handleBrand(Product product, String brandName)
    {
        Brand brand = null;
        if (brandName.equals("")) {
            brandName = "_Generic";
        }

//    brand = brandsController.brandSelectFromName(brandName);
        brand = containsBrand(brandName);
        if (brand == null) {
            brand = brandsController.brandCreate(brandName);
            if (brand == null) {
                RetailFxImporter.getMainController().appendOutputMessage("Error inserting brand: " + brandName);
                return false;
            }
            getBrands();
        }

        product.setBrand(brand);

        return true;
    }

    private boolean handleSizeUom(Product product, String sizeAndUom)
    {
        sizeAndUom = sizeAndUom.toLowerCase();
        sizeAndUom = sizeAndUom.replace(" ", "");

        if (sizeAndUom.equals("") || sizeAndUom.endsWith("lb")) {
            sizeAndUom = "1ea";
        }

        if (sizeAndUom.startsWith(".")) {
            sizeAndUom = "0" + sizeAndUom;
        } else if (!sizeAndUom.matches("\\d.+")) {
            sizeAndUom = "1" + sizeAndUom;
        }

        if (sizeAndUom.equals("") || !sizeAndUom.matches("\\d.+")) {
            RetailFxImporter.getMainController().appendOutputMessage("invalid(b) uom: " + "[" + product.getItemNumber() + " -- " + sizeAndUom + "]");
            return false;
        }

        sizeAndUom = sizeAndUom.replace("v-cp", "vcap");

        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(sizeAndUom);
        boolean foundSize = m.find();

        if (!foundSize) {
            RetailFxImporter.getMainController().appendOutputMessage("invalid(b) size: " + "[" + product.getItemNumber() + " -- " + sizeAndUom + "]");
            return false;
        }
        int size = Integer.valueOf(m.group(0));
        product.setSize(size);

        Matcher matcher = Pattern.compile("[a-zA-z]+").matcher(sizeAndUom);
        boolean foundUom = matcher.find();

        if (!foundUom) {
            RetailFxImporter.getMainController().appendOutputMessage("invalid(b) uom: " + "[" + product.getItemNumber() + " -- " + sizeAndUom + "]");
            return false;
        }

        String uomCode = matcher.group();

//    Uom uom = uomController.uomSelectFromCode(uomCode);
        Uom uom = containsUom(uomCode);
        if (uom == null) {
            uom = uomController.createUom("", uomCode);
            if (uom == null) {
                RetailFxImporter.getMainController().appendOutputMessage("Error inserting uom: " + uomCode);
                return true;
            }
            getUoms();
        }

        product.setUom(uom);

        return true;
    }

    private boolean handleCategory(Product product, String fullCategoryCode)
    {
        if (fullCategoryCode.length() != 8) {
            RetailFxImporter.getMainController().appendOutputMessage("Invalid category: " + product.getItemNumber() + " -- " + fullCategoryCode);
            return false;
        }

        Category category = containsCategory(fullCategoryCode);
        if (category == null) {
            RetailFxImporter.getMainController().appendOutputMessage("Error finding category: " + product.getItemNumber() + " -- " + fullCategoryCode);
            return false;
        }

        product.setCategory(category);

        return true;
    }

    private boolean handleItemName_Vendor(Product product, String itemName, String vendorCode, String vendorItemNumber, String sizeAndUom, ProductVendor productVendor, ProductCompany productCompany)
    {
        int productAvailabilityId = 1;
        int vendorAvailabilityId = 1;
        if (itemName.toUpperCase().startsWith("DNS")) {
            if (itemName.toUpperCase().startsWith("DNS ")) {
                itemName = itemName.substring(itemName.indexOf(" ") + 1);
            } else {
                itemName = itemName.substring(itemName.indexOf("S") + 1);
            }

            productAvailabilityId = 3;
        } else if (itemName.toUpperCase().startsWith("DISC")) {
            if (itemName.toUpperCase().startsWith("DISC ")) {
                itemName = itemName.substring(itemName.indexOf(" ") + 1);
            } else {
                itemName = itemName.substring(itemName.indexOf("C") + 1);
            }

            productAvailabilityId = 2;
        } else if (itemName.toUpperCase().startsWith("***")) {
            if (itemName.toUpperCase().startsWith("*** ")) {
                itemName = itemName.substring(itemName.indexOf(" ") + 1);
            } else {
                String substring = "***";
                int index = itemName.indexOf("Hello") + substring.length() + 1;
                itemName = itemName.substring(index);
            }
            vendorAvailabilityId = 4; //seasonal
        } else if (itemName.toUpperCase().startsWith("DNO")) {
            if (itemName.toUpperCase().startsWith("DNO ")) {
                itemName = itemName.substring(itemName.indexOf(" ") + 1);
            } else {
                itemName = itemName.substring(itemName.indexOf("O") + 1);
            }

            productAvailabilityId = 4; //unavilable
        }

        product.setName(itemName);

//    ProductAvailability productAvailability = availabilityController.selectProductAvailabilityFromId(productAvailabilityId);
//    ProductVendorAvailability vendorAvailability = availabilityController.selectProductVendorAvailabilityFromId(vendorAvailabilityId);
        productCompany.getAvailability().setAvailabilityId(productAvailabilityId);
        productVendor.getAvailability().setAvailabilityId(vendorAvailabilityId);

        sizeAndUom = sizeAndUom.replace(" ", "");
        sizeAndUom = sizeAndUom.replace("v-cp", "vcap");
        int caseSize = 1;
        if (sizeAndUom.contains("-")) {
            try {
                String caseSizeString = sizeAndUom.substring(sizeAndUom.lastIndexOf("-") + 1).toLowerCase();

                if (caseSizeString.matches("\\d+\\w+")) {
                    Pattern p = Pattern.compile("\\d+");
                    Matcher m = p.matcher(sizeAndUom);
                    boolean foundSize = m.find();
                    if (foundSize) {
                        caseSize = Integer.valueOf(m.group(0));
                    } else {
                        caseSize = 1;
                    }
                } else if (Arrays.asList("ea", "e", "each", "bg", "bag", "bx", "box", "piece").contains(caseSizeString) || caseSizeString.endsWith("lb")) {
                    caseSize = 1;
                } //        else if(caseSizeString.endsWith("cs"))
                //        {
                //          caseSize = Integer.valueOf(caseSizeString.substring(0, caseSizeString.indexOf("cs")));
                //        }
                //        else if(caseSizeString.endsWith("qt") || caseSizeString.endsWith("qty"))
                //        {
                //          caseSize = Integer.valueOf(caseSizeString.substring(0, caseSizeString.indexOf("qt")));
                //        }
                else {
                    caseSize = Integer.valueOf(caseSizeString);
                }
            } catch (NumberFormatException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                RetailFxImporter.getMainController().appendOutputMessage("Error formatting case size: " + product.getItemNumber() + " -- " + sizeAndUom);
                return false;
            }
        }

        Vendor vendor = null;
        if (vendorCode.equals("")) {
//      vendor = vendorsController.vendorSelectFromCode("ZZZ");
            vendor = containsVendor("ZZZ");
            if (vendor == null) {
                vendor = vendorsController.createNullVendor();
                vendor.setVendorCode("ZZZ");
                vendor.setVendorName("Various");
                boolean insert = vendorsController.vendorInsert(vendor);
                if (!insert) {
                    RetailFxImporter.getMainController().appendOutputMessage("Error inserting generic vendor");
                    return false;
                }
                getVendors();
            }
        }

        if (vendor == null) {
//      vendor = vendorsController.vendorSelectFromCode(vendorCode);
            vendor = containsVendor(vendorCode);
            if (vendor == null) {
                RetailFxImporter.getMainController().appendOutputMessage("Error finding vendor: " + product.getItemNumber() + " -- " + vendorCode);
                return false;
            }
        }

        productCompany.setProduct(product);
        productCompany.setPrimaryVendor(vendor);
//    productCompany.setProductAvailability(productAvailability);

        productVendor.setProduct(product);
        productVendor.setVendor(vendor);
        productVendor.setVendorProductNumber(vendorItemNumber);
        productVendor.setCaseSize(caseSize);
//    productVendor.setAvailability(vendorAvailability);

        return true;
    }

    private void insertPricing(Product product, double cost, double salePrice)
    {
//    ArrayList<Company> companyList = locationController.companySelectAll();

        for (Company company : companyList) {
            Price price = pricesController.createPrice(product, company, cost, 0.0, 0.0, salePrice, false);
//      if(price == null){System.err.println("Error inserting product pricing: " + product.getName()); System.exit(1); }
            pricesInsertList.add(pricesController.priceInsertStringForBatch(price));
        }
    }

    private void insertInventory(Product product)
    {
//    ArrayList<Store> storeList = locationController.storeSelectAll();

        for (Store store : storeList) {
            Inventory inventory = inventoryController.createInventory(product, store, 0, 0, 0, false);
//      if(inventory == null) {System.err.println("Error inserting product inventory: " + product.getName()); System.exit(1); }
            inventoryInsertList.add(inventoryController.inventoryInsertStringForBatch(inventory));
        }
    }

    private void insertProductVendors(ProductVendor productVendor)
    {
//    ArrayList<Company> companyList = locationController.companySelectAll();
        for (Company company : companyList) {
            productVendor.setCompany(company);
//      boolean insert = productsController.productVendorInsert(productVendor, company);
//      if(!insert) {System.err.println("Error inserting productVendor: " + productVendor.getProduct().getName() + " - " + productVendor.getVendor().getVendorName()); System.exit(1); }
            productVendorInsertList.add(productsController.productVendorInsertStringForBatch(productVendor, company));
        }
    }

    private void insertProductCompanies(ProductCompany productCompany)
    {
//    ArrayList<Company> companyList = locationController.companySelectAll();
        for (Company company : companyList) {
            productCompany.setCompany(company);
//      boolean insert = productsController.productCompanyInsert(productCompany, company);
//      if(!insert) {System.err.println("Error inserting productCompany: " + productCompany.getProduct().getName() + " - " + company.getCompanyName()); System.exit(1); }
            productCompanyInsertList.add(productsController.productCompanyInsertStringForBatch(productCompany, company));
        }
    }

    private void getCompanies()
    {
        companyList = locationController.companySelectAll();
    }

    private void getStores()
    {
        storeList = locationController.storeSelectAll();
    }

    private void getVendors()
    {
        vendorList = vendorsController.vendorSelect();
        vendorTM = new TreeMap<>();

        for (Vendor vendor : vendorList) {
            vendorTM.put(vendor.getVendorCode(), vendor);
        }
    }

    private Vendor containsVendor(String vendorCode)
    {
        if (vendorTM.containsKey(vendorCode)) {
            return vendorTM.get(vendorCode);
        }
        return null;
    }

    private Brand containsBrand(String brandName)
    {
        brandName = brandName.toLowerCase();
        if (brandTM.containsKey(brandName)) {
            return brandTM.get(brandName);
        }
        return null;
    }

    private void getBrands()
    {
        brandList = brandsController.brandSelect();

        brandTM = new TreeMap<>();
        for (Brand brand : brandList) {
            brandTM.put(brand.getBrandName().toLowerCase(), brand);
        }
    }

    private void getCategories()
    {
        categorylist = categoriesController.categorySelect();

        categoryTM = new TreeMap<>();
        for (Category category : categorylist) {
            categoryTM.put(
                    category.getPath().replace(":", ""),
                    category);
        }
    }

    private Category containsCategory(String dcs)
    {
        if (categoryTM.containsKey(dcs)) {
            return categoryTM.get(dcs);
        }
        return null;
    }

    private void getUoms()
    {
        uomList = uomController.uomSelect();

        uomTM = new TreeMap<>();
        for (Uom uom : uomList) {
            uomTM.put(uom.getUomCode().toLowerCase(), uom);
        }
    }

    private Uom containsUom(String uomCode)
    {
        uomCode = uomCode.toLowerCase();
        if (uomTM.containsKey(uomCode)) {
            return uomTM.get(uomCode);
        }
        return null;
    }
}
