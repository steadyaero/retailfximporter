/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters.retailpro.parsers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.aeroaddiction.erpsqllibrary.Controllers.UomController;
import com.aeroaddiction.erpsqllibrary.classes.Uom;
import com.aeroaddiction.retailfximporter.RetailFxImporter;

/**
 *
 * @author rjones
 */
public class UomParcer
{

    private List<String[]> fileContents;
    private final UomController uomController;
    private ArrayNode arrayNode;

    public UomParcer(List<String[]> fileContents)
    {
        this.fileContents = fileContents;
        uomController = new UomController(RetailFxImporter.sqlClass);
    }

    public UomParcer(ArrayNode arrayNode)
    {
        this.arrayNode = arrayNode;
        uomController = new UomController(RetailFxImporter.sqlClass);
    }

    public void processJson()
    {
        TreeSet<String> outputTS = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
            ObjectNode objectNode = (ObjectNode) it.next();
            String sizeAndType = objectNode.get("Size").textValue();
            String itemNumber = objectNode.get("Item #").textValue();
            String uomCode = processLine(sizeAndType, itemNumber);
            outputTS.add(uomCode);
        }

        for (String uomCode : outputTS) {
            Uom uom = uomController.createUom("", uomCode);
            if (uom == null) {
                System.err.println("Error inserting uom: " + uomCode);
                System.exit(1);
            }
        }
    }

    private String processLine(String sizeAndType, String itemNumber)
    {
        sizeAndType = sizeAndType.toLowerCase();

        if (sizeAndType.equals("")) {
            sizeAndType = "1ea";
        }

        if (sizeAndType.startsWith(".")) {
            sizeAndType = "0" + sizeAndType;
        } else if (!sizeAndType.matches("\\d.+")) {
            sizeAndType = "1" + sizeAndType;
        }

        if (sizeAndType.equals("") || !sizeAndType.matches("\\d.+")) {
            System.out.println("invalid(b) uom: " + "[" + itemNumber + " -- " + sizeAndType + "]");
            System.exit(1);
        }

        Matcher matcher = Pattern.compile("[a-zA-z]+").matcher(sizeAndType);
        boolean found = matcher.find();

        if (!found) {
            System.out.println("invalid(b) uom: " + "[" + itemNumber + " -- " + sizeAndType + "]");
            System.exit(1);
        }

        String uomCode = matcher.group();

        return uomCode;
    }
}
