/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters.retailpro.parsers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Randy
 */
public class PricingParcer
{

  private final List<String[]> fileContents;

  public PricingParcer(List<String[]> fileContents)
  {
    this.fileContents = fileContents;
  }
  
  
  public ArrayList<String[]> fileParser()
  {
    ArrayList<String[]> outputLineAL = new ArrayList<>();
    outputLineAL.add(getHeader());
    int i=3138;
//    System.out.println("Processing line: " + i);
    for(String[] lineContentRaw : fileContents)
//    for(int j=i; j<fileContents.size(); j++) //temp restarter
    {
//      String[] lineContentRaw = fileContents.get(j);
      String[] lineContent = new String[20];
      Arrays.fill(lineContent, "");
      System.arraycopy(lineContentRaw, 0, lineContent, 0, lineContentRaw.length);
//      System.out.println("Processing line: " + i);
      if(!lineContent[1].equals(""))
      {
        String[] csvLine = processLine(lineContent);
        outputLineAL.add(csvLine);
      }
      i++;
//      if(i%50 == 0)
//        System.out.println("Processing line: " + i);
    }
    return outputLineAL;
  }
  
  private String[] getHeader()
  {
    ArrayList<String> header = new ArrayList<>();
    header.add("upc");
    header.add("itemNum");
    header.add("brand");
    header.add("name");
    header.add("size");
    header.add("uomCode");
    header.add("categoryId");
    header.add("categoryCode");
    
    String[] content = header.toArray(new String[header.size()]);
    return content;
  }
  
  private String[] processLine(String[] lineContent)
  {
    String itemNum = lineContent[0];
    String cost = lineContent[8];
    String invenP = lineContent[9];
    String marginPct = lineContent[10];
    
    ArrayList<String> finalLineContent = new ArrayList<>();
    
    
    String[] content = finalLineContent.toArray(new String[finalLineContent.size()]);
    
    return content;
  }
}
