/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters.retailpro.parsers;

import com.aeroaddiction.erpsqllibrary.Controllers.InventoryController;
import com.aeroaddiction.erpsqllibrary.Controllers.LocationController;
import com.aeroaddiction.erpsqllibrary.Controllers.ProductController;
import com.aeroaddiction.erpsqllibrary.classes.Inventory;
import com.aeroaddiction.erpsqllibrary.classes.Product;
import com.aeroaddiction.erpsqllibrary.classes.Store;
import com.aeroaddiction.retailfximporter.RetailFxImporter;
import com.aeroaddiction.retailfximporter.SqlController;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Randy
 */
public class InventoryParser
{
    private LocationController locationController;
    private ArrayList<Store> storeList;
    private final ArrayList<String> inventoryInsertList;
    private ProductController productController;
    private final ArrayNode arrayNode;
    private ArrayList<Product> productList;
    private Map<Integer, Product> productMap;
    private Map<String, Store> storeMap;
    private InventoryController inventoryController;
    private SqlController sqlController;

    public InventoryParser(ArrayNode arrayNode)
    {
        this.arrayNode = arrayNode;
        initControllers();
        getStores();
        getProducts();
        inventoryInsertList = new ArrayList<>();
    }

    private void initControllers()
    {
        locationController = new LocationController(RetailFxImporter.sqlClass);
        productController = new ProductController(RetailFxImporter.sqlClass);
        inventoryController = new InventoryController(RetailFxImporter.sqlClass);

        sqlController = new SqlController();
    }

    private void getStores()
    {
        storeList = locationController.storeSelectAll();
        storeMap = storeList.stream().collect(Collectors.toMap(Store::getStoreCode, item -> item));
    }

    private void getProducts()
    {
        productList = productController.selectProducts();
        productMap = productList.stream().collect(Collectors.toMap(Product::getItemNumber, item -> item));
    }

    public boolean processJson()
    {
        int i = 0;
        for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
            i++;
            if (i % 1000 == 0) {
                RetailFxImporter.getMainController().appendOutputMessage("processing line: " + i);
            }

            ObjectNode objectNode = (ObjectNode) it.next();
            String storeCode = objectNode.get("Store Code").textValue();
            String itemNumberStr = objectNode.get("Item #").textValue();
            String qtyStr = objectNode.get("Qty").textValue();
            String minStr = objectNode.get("Min").textValue();
            String maxStr = objectNode.get("Max").textValue();

            if (qtyStr.equals("")) {
                continue;
            }

            int itemNumber = Integer.valueOf(itemNumberStr);
            int qty = Integer.valueOf(qtyStr);
            int min = Integer.valueOf(minStr);
            int max = Integer.valueOf(maxStr);

            Product product = productMap.get(itemNumber);
            if (product == null) {
                continue;
            }
            Store store = storeMap.get(storeCode);
            insertInventory(product, store, qty, min, max);
        }

        return batchInsert();
    }

    private void insertInventory(Product product, Store store, int qty, int min, int max)
    {
        Inventory inventory = inventoryController.createInventory(product, store, qty, min, max, false);
        inventoryInsertList.add(inventoryController.inventoryInsertStringForBatch(inventory));
    }

    private boolean batchInsert()
    {
        RetailFxImporter.getMainController().appendOutputMessage("Inserting inventory");
        boolean inventoryInsert = sqlController.batchInsert(inventoryInsertList, "inventory");
        inventoryInsertList.clear();
        if (!inventoryInsert) {
            RetailFxImporter.getMainController().appendOutputMessage("Error inserting inventory");
            return false;
        }

        return true;
    }
}
