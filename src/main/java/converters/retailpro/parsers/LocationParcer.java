/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters.retailpro.parsers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.aeroaddiction.erpsqllibrary.classes.Company;
import java.util.Iterator;
import com.aeroaddiction.erpsqllibrary.Controllers.LocationController;
import com.aeroaddiction.erpsqllibrary.classes.CompanyConfig;
import com.aeroaddiction.erpsqllibrary.classes.Store;
import com.aeroaddiction.erpsqllibrary.classes.StoreAddress;
import com.aeroaddiction.erpsqllibrary.classes.StoreConfig;
import com.aeroaddiction.retailfximporter.RetailFxImporter;
import java.util.ArrayList;

/**
 *
 * @author rjones
 */
public class LocationParcer
{

    ArrayList<Company> companies = new ArrayList<>();
    ArrayList<Store> stores = new ArrayList<>();
    ArrayList<StoreAddress> storeAddresses = new ArrayList<>();

    private LocationController locationController;
    private final ArrayNode arrayNode;

    public LocationParcer(ArrayNode arrayNode)
    {
        this.arrayNode = arrayNode;
        initController();
    }

    private void initController()
    {
        locationController = new LocationController(RetailFxImporter.sqlClass);
    }

    public boolean processJson()
    {
        for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
            ObjectNode objectNode = (ObjectNode) it.next();
            String companyName = objectNode.get("Company Name").textValue();
            String companyCode = objectNode.get("Company Code").textValue();
            String storeName = objectNode.get("Store Name").textValue();
            String storeCode = objectNode.get("Store Code").textValue();
            String address = objectNode.get("Address").textValue();
            String city = objectNode.get("City").textValue();
            String state = objectNode.get("State").textValue();
            String zipCode = objectNode.get("Zip Code").textValue();
            String taxRateStr = objectNode.get("Tax Rate").textValue();

            double taxRate = 0.0;
            if (!taxRateStr.isEmpty()) {
                taxRate = Double.valueOf(taxRateStr);
            }

            Company company = new Company(0, companyName, companyCode);
            if (!companies.stream().filter(o -> o.getCompanyCode().equals(companyCode)).findFirst().isPresent()) {
                companies.add(company);
            }

            Store store = new Store(0, storeCode, storeName, taxRate, company);
            StoreAddress storeAddress = new StoreAddress(0, address, city, state, zipCode, store);
            stores.add(store);
            storeAddresses.add(storeAddress);
        }

        return insert();
    }

    private boolean insert()
    {
        for (Company company : companies) {
            boolean insert = locationController.companyInsert(company);
            if (!insert) {
                RetailFxImporter.getMainController().appendOutputMessage("Error inserting company: " + company.getCompanyName());
                return false;
            }

            CompanyConfig companyConfig = new CompanyConfig(0, "Welcome to " + company.getCompanyName(), "Thank you for shopping at " + company.getCompanyName(), false, false, company);
            boolean insertConfig = locationController.companyConfigInsert(companyConfig);
            if (!insertConfig) {
                RetailFxImporter.getMainController().appendOutputMessage("Error inserting company config: " + company.getCompanyName());
                return false;
            }
        }

        for (Store store : stores) {
            if (companies.stream().filter(o -> o.getCompanyCode().equals(store.getCompany().getCompanyCode())).findFirst().isPresent()) {
                store.setCompany(companies.stream().filter(o -> o.getCompanyCode().equals(store.getCompany().getCompanyCode())).findFirst().get());
            }
            boolean insert = locationController.storeInsert(store);
            if (!insert) {
                RetailFxImporter.getMainController().appendOutputMessage("Error inserting store: " + store.getCompany().getCompanyName() + "-" + store.getStoreName());
                RetailFxImporter.getMainController().appendOutputMessage(locationController.getException().getMessage());
                return false;
            }

            StoreConfig storeConfig = new StoreConfig(0, 1, 1, store);
            boolean insertConfig = locationController.storeConfigInsert(storeConfig);
            if (!insertConfig) {
                RetailFxImporter.getMainController().appendOutputMessage("Error inserting store config: " + store.getCompany().getCompanyName() + "-" + store.getStoreName());
                return false;
            }
        }

//        for (StoreAddress storeAddress : storeAddresses) {
//            boolean insertAddress = locationController.storeAddressInsert(storeAddress);
//            if (!insertAddress) {
//                RetailFxImporter.getMainController().appendOutputMessage("Error inserting store address: " + storeAddress.getStore().getCompany().getCompanyName() + "-" + storeAddress.getStore().getStoreName());
//                return false;
//            }
//        }
        return true;
    }
}
