/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters.retailpro.parsers;

import com.aeroaddiction.erpsqllibrary.Controllers.CategoryController;
import com.aeroaddiction.erpsqllibrary.classes.Category;
import com.aeroaddiction.retailfximporter.RetailFxImporter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Iterator;
import java.util.TreeMap;

/**
 *
 * @author rjones
 */
public class CategoryParser
{

    boolean _debug = false;
    TreeMap<String, Object> categoriesTM = new TreeMap<>();
    ArrayNode categories;

    private final CategoryController categoriesController;
    private final ArrayNode arrayNode;
    private ObjectMapper mapper;

    public CategoryParser(ArrayNode arrayNode)
    {
        this.arrayNode = arrayNode;
        categoriesController = new CategoryController(RetailFxImporter.sqlClass);

        initCategoryArray();
    }

    public boolean processJson()
    {
        for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
            ObjectNode objectNode = (ObjectNode) it.next();
            processLine3(objectNode);
        }

        return insertSql();
    }

    private void initCategoryArray()
    {
        mapper = new ObjectMapper();
        categories = mapper.createArrayNode();
    }

    private void processLine3(ObjectNode objectNode)
    {
        String dcsName = objectNode.get("DCS Name").textValue();
        String deptCode = objectNode.get("Dept").textValue();
        String classCode = objectNode.get("Class").textValue();
        String subclassCode = objectNode.get("Subclass").textValue();
        String taxableStr = objectNode.get("Tax").textValue();
        boolean taxable = taxableStr.equals("Taxable");

        String deptName = dcsName.substring(0, 10).trim();
        String className = dcsName.substring(10, 20).trim();
        String subclassName = dcsName.substring(20).trim();

        if (!categoriesTM.containsKey(deptCode)) {
            TreeMap<String, String> subclassTm = new TreeMap<>();
            subclassTm.put("name", subclassName);
            subclassTm.put("code", subclassCode);
            subclassTm.put("taxable", String.valueOf(taxable));

            TreeMap<String, Object> subclassCollection = new TreeMap<>();
            subclassCollection.put(subclassCode, subclassTm);

            TreeMap<String, Object> classTM = new TreeMap<>();
            classTM.put("name", className);
            classTM.put("code", classCode);
            classTM.put("subclasses", subclassCollection);

            TreeMap<String, Object> classCollection = new TreeMap<>();
            classCollection.put(classCode, classTM);

            TreeMap<String, Object> departmentTM = new TreeMap<>();
            departmentTM.put("name", deptName);
            departmentTM.put("code", deptCode);
            departmentTM.put("classes", classCollection);

            categoriesTM.put(deptCode, departmentTM);
        } else {
            TreeMap<String, Object> departmentTM = (TreeMap<String, Object>) categoriesTM.get(deptCode);
            TreeMap<String, Object> classCollection = (TreeMap<String, Object>) departmentTM.get("classes");
            if (!classCollection.containsKey(classCode)) {
                TreeMap<String, String> subclassTm = new TreeMap<>();
                subclassTm.put("name", subclassName);
                subclassTm.put("code", subclassCode);
                subclassTm.put("taxable", String.valueOf(taxable));

                TreeMap<String, Object> subclassCollection = new TreeMap<>();
                subclassCollection.put(subclassCode, subclassTm);

                TreeMap<String, Object> classTM = new TreeMap<>();
                classTM.put("name", className);
                classTM.put("code", classCode);
                classTM.put("subclasses", subclassCollection);

                classCollection.put(classCode, classTM);
            } else {
                TreeMap<String, Object> classTM = (TreeMap<String, Object>) classCollection.get(classCode);
                TreeMap<String, Object> subclassCollection = (TreeMap<String, Object>) classTM.get("subclasses");
                if (!subclassCollection.containsKey(subclassCode)) {
                    TreeMap<String, String> subclassTm = new TreeMap<>();
                    subclassTm.put("name", subclassName);
                    subclassTm.put("code", subclassCode);
                    subclassTm.put("taxable", String.valueOf(taxable));

                    subclassCollection.put(subclassCode, subclassTm);
                }
            }
        }
    }

    private boolean insertSql()
    {
        for (String deptCode : categoriesTM.keySet()) {
            TreeMap<String, Object> departmentTM = (TreeMap<String, Object>) categoriesTM.get(deptCode);
            String deptName = (String) departmentTM.get("name");

            Category department = categoriesController.createCategory(deptCode, deptName, false, null);
            if (department == null) {
                RetailFxImporter.getMainController().appendOutputMessage("Error inserting category department: " + deptName);
                return false;
            }

            TreeMap<String, Object> classCollection = (TreeMap<String, Object>) departmentTM.get("classes");
            for (String classCode : classCollection.keySet()) {
                TreeMap<String, Object> classTM = (TreeMap<String, Object>) classCollection.get(classCode);
                String className = (String) classTM.get("name");

                Category categoryClass = categoriesController.createCategory(classCode, className, false, department.getCategoryId());
                if (categoryClass == null) {
                    RetailFxImporter.getMainController().appendOutputMessage("Error inserting category class: " + department.getCategoryName() + "-" + className);
                    return false;
                }

                TreeMap<String, Object> subclassCollection = (TreeMap<String, Object>) classTM.get("subclasses");
                for (String subclassCode : subclassCollection.keySet()) {
                    TreeMap<String, Object> subclassTM = (TreeMap<String, Object>) subclassCollection.get(subclassCode);
                    String subclassName = (String) subclassTM.get("name");
                    boolean taxable = Boolean.valueOf((String) subclassTM.get("taxable"));

                    Category subclass = categoriesController.createCategory(subclassCode, subclassName, taxable, categoryClass.getCategoryId());
                    if (subclass == null) {
                        RetailFxImporter.getMainController().appendOutputMessage("Error inserting subclass: " + department.getCategoryName() + "-" + categoryClass.getCategoryName() + "-" + subclassName);
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
