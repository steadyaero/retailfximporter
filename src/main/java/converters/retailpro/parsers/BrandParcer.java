/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters.retailpro.parsers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Iterator;
import java.util.TreeSet;
import com.aeroaddiction.erpsqllibrary.Controllers.BrandController;
import com.aeroaddiction.erpsqllibrary.classes.Brand;
import com.aeroaddiction.retailfximporter.RetailFxImporter;

/**
 *
 * @author rjones
 */
public class BrandParcer
{

    private final BrandController brandsController;
    private final ArrayNode arrayNode;

    public BrandParcer(ArrayNode arrayNode)
    {
        this.arrayNode = arrayNode;
        brandsController = new BrandController(RetailFxImporter.sqlClass);
    }

    public boolean processJson()
    {
        TreeSet<String> outputTS = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
            ObjectNode objectNode = (ObjectNode) it.next();
            String brandName = objectNode.get("Desc 3").textValue();
            if (brandName.equals("")) {
                continue;
            }

            outputTS.add(brandName);
        }

        for (String brandName : outputTS) {
            Brand brand = brandsController.brandCreate(brandName);
            if (brand == null) {
                RetailFxImporter.getMainController().appendOutputMessage("Error inserting brand: " + brandName);
                return false;
            }
        }

        return true;
    }
}
