/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters.retailpro;

import com.fasterxml.jackson.databind.node.ArrayNode;
import excel.Reader;
import java.io.File;
import com.aeroaddiction.erpsqllibrary.Controllers.LocationController;
import com.aeroaddiction.erpsqllibrary.Controllers.UserController;
import com.aeroaddiction.erpsqllibrary.classes.Company;
import com.aeroaddiction.erpsqllibrary.classes.User;
import com.aeroaddiction.erpsqllibrary.classes.UserRole;
import com.aeroaddiction.retailfximporter.RetailFxImporter;
import converters.retailpro.parsers.BrandParcer;
import converters.retailpro.parsers.InventoryParser;
import converters.retailpro.parsers.LocationParcer;
import converters.retailpro.parsers.ProductParcer;
import converters.retailpro.parsers.VendorParcer;
import converters.retailpro.parsers.CategoryParser;

/**
 *
 * @author rjones
 */
public class RetailPro
{
    private final File sourceDataFolder;

    public RetailPro(File sourceDataFolder)
    {
        this.sourceDataFolder = sourceDataFolder;
    }

    File directory = new File("C:\\Users\\Randy\\Desktop\\Dat\\");

    public boolean convert()
    {
        RetailFxImporter.getMainController().appendOutputMessage("Starting RetailPro conversion");
        boolean result = true;
        result &= processXlsx();
        if (!result) {
            return false;
        }
        result &= insertAdminUsers();

        return result;
    }

    private boolean processXlsx()
    {
        boolean result = true;
        RetailFxImporter.getMainController().appendOutputMessage("Reading excel files into memory as JSON objects");
        ArrayNode inventoryList = Reader.readXLSX(sourceDataFolder.getAbsolutePath() + "\\Inventory.xlsx", true);
        ArrayNode departmentList = Reader.readXLSX(sourceDataFolder.getAbsolutePath() + "\\Departments.xlsx", true);
        ArrayNode vendorList = Reader.readXLSX(sourceDataFolder.getAbsolutePath() + "\\Vendors.xlsx", true);
        ArrayNode locationList = Reader.readXLSX(sourceDataFolder.getAbsolutePath() + "\\Locations.xlsx", true);
        ArrayNode storeInventoryList = Reader.readXLSX(sourceDataFolder.getAbsolutePath() + "\\store_inventory.xlsx", true);

        RetailFxImporter.getMainController().appendOutputMessage("Processing locations");
        LocationParcer locations = new LocationParcer(locationList);
        result &= locations.processJson();

        if (!result) {
            return false;
        }

        RetailFxImporter.getMainController().appendOutputMessage("Processing vendors");
        VendorParcer vendors = new VendorParcer(vendorList);
        result &= vendors.processJson();

        if (!result) {
            return false;
        }

        RetailFxImporter.getMainController().appendOutputMessage("Processing categories");
        CategoryParser departments = new CategoryParser(departmentList);
        result &= departments.processJson();

        if (!result) {
            return false;
        }

        RetailFxImporter.getMainController().appendOutputMessage("Processing brands");
        BrandParcer brands = new BrandParcer(inventoryList);
        result &= brands.processJson();

        if (!result) {
            return false;
        }

        RetailFxImporter.getMainController().appendOutputMessage("Processing products");
        ProductParcer products = new ProductParcer(inventoryList);
        result &= products.processJson();

        if (!result) {
            return false;
        }

        RetailFxImporter.getMainController().appendOutputMessage("Processing inventory");
        InventoryParser inventory = new InventoryParser(storeInventoryList);
        result &= inventory.processJson();

        return result;
    }

    private boolean insertAdminUsers()
    {
        LocationController locationController = new LocationController(RetailFxImporter.sqlClass);
        UserController userController = new UserController(RetailFxImporter.sqlClass);

        UserRole role = userController.selectUserRoleFromId(1);

        for (Company company : locationController.companySelectAll()) {
            User user = userController.userCreate(company.getCompanyCode() + ".Admin", "admin", company.getCompanyCode(), "Admin", company, role);
            if (user == null) {
                return false;
            }
        }

        return true;
    }
}
