/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroaddiction.retailfximporter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.dbcp2.BasicDataSource;

/**
 *
 * @author Randy
 */
public class SqlController
{

    private final BasicDataSource bds;
    private Exception exception;

    /**
     *
     */
    public SqlController()
    {
        bds = RetailFxImporter.sqlClass.getDataSource(false);
    }

    public Exception getException()
    {
        Exception ex = this.exception;
        this.exception = null;
        return ex;
    }

    /**
     *
     * @return
     */
    public ArrayList<String> getViews()
    {
        ArrayList<String> viewAl = new ArrayList<>();
        String query = ""
                       + "SELECT table_name "
                       + "FROM information_schema.views "
                       + "WHERE table_schema = ?";
        try (
                Connection conn = bds.getConnection();
                PreparedStatement ps = conn.prepareCall(query);) {
            ps.setString(1, RetailFxImporter.sqlClass.getDatabase());
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                viewAl.add(rs.getString(1));
            }

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            this.exception = ex;
        }

        return viewAl;
    }

    /**
     *
     * @return
     */
    public ArrayList<String> getRoutines()
    {
        ArrayList<String> routineAl = new ArrayList<>();
        String query = ""
                       + "SELECT routine_name "
                       + "FROM information_schema.routines "
                       + "WHERE routine_schema = ?";
        try (
                Connection conn = bds.getConnection();
                PreparedStatement ps = conn.prepareCall(query);) {
            ps.setString(1, RetailFxImporter.sqlClass.getDatabase());
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                routineAl.add(rs.getString(1));
            }

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            this.exception = ex;
        }

        return routineAl;
    }

    /**
     *
     * @return
     */
    public ArrayList<String> getTables()
    {
        ArrayList<String> tableAl = new ArrayList<>();
        String query = ""
                       + "SELECT table_name "
                       + "FROM information_schema.tables "
                       + "WHERE table_schema = ? "
                       + "AND table_type='BASE TABLE'";
        try (
                Connection conn = bds.getConnection();
                PreparedStatement ps = conn.prepareCall(query);) {
            ps.setString(1, RetailFxImporter.sqlClass.getDatabase());
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                tableAl.add(rs.getString(1));
            }

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            this.exception = ex;
        }

        return tableAl;
    }

    /**
     *
     * @param insertList
     * @param collectionName
     * @return
     */
    public boolean batchInsert(ArrayList<String> insertList, String collectionName)
    {
        String concatenatedString = String.join("\n", insertList);
        concatenatedString += ";";

        try (
                Connection conn = bds.getConnection();
                PreparedStatement ps = conn.prepareStatement(concatenatedString);) {
//            currentStatement = ps;
            int i = 0;
            for (String string : insertList) {
                ps.addBatch(string);
                i++;
                if (i % 1000 == 0 || i == insertList.size()) {
                    RetailFxImporter.getMainController().appendOutputMessage("Inserting " + collectionName + ": " + i);
                    ps.executeBatch(); // Execute every 1000 items.
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SqlHandler.class.getName()).log(Level.SEVERE, null, ex);
            this.exception = ex;
            RetailFxImporter.getMainController().appendOutputMessage(ex.getMessage());
            Dialogs.showException("Error Batch Inserting Data", ex);
            return false;
        }

        return true;
    }
}
