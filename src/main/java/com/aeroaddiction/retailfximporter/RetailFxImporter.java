package com.aeroaddiction.retailfximporter;

import com.aeroaddiction.retailfximporter.controllers.MainController;
import com.aeroaddiction.sqlconnector.SqlClass;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.Optional;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.input.DataFormat;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class RetailFxImporter extends Application
{
    public static final String FXML_PATH = "/fxml/";
    public static final String STYLES_PATH = "/styles/";
    private static Stage rootStage;
    public static boolean debugging;
    public static SqlClass sqlClass;
    private static MainController mainController;
    public static String ORDERED_TABLES = System.getenv("PROGRAMDATA") + File.separator + "RetailFxImporter" + File.separator + "StaticTables.txt";
    public static DataFormat dataFormat = new DataFormat("mycell");

    @Override
    public void start(Stage stage) throws Exception
    {
        debugging = ManagementFactory.getRuntimeMXBean().getInputArguments().toString().contains("jdwp");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(RetailFxImporter.FXML_PATH + "Main.fxml"));

        Parent root = loader.load();
        mainController = loader.getController();

        Scene scene = new Scene(root);
        scene.getStylesheets().add(RetailFxImporter.STYLES_PATH + "Styles.css");

        RetailFxImporter.rootStage = stage;

        stage.setTitle("RetailFX Importer");
        stage.setScene(scene);
        stage.show();
    }

    public static Stage getRootStage()
    {
        return RetailFxImporter.rootStage;
    }

    public static MainController getMainController()
    {
        return RetailFxImporter.mainController;
    }

    public static EventHandler<WindowEvent> confirmCloseEventHandler = event
            -> {
        Optional<ButtonType> closeResponse = Dialogs.showConfirmation("Confirm Exit", "Are you sure you want to exit?", "");

        if (!ButtonType.OK.equals(closeResponse.get())) {
            event.consume();
        } else {
            Platform.exit();
        }
    };

    /**
     * The main() method is ignored in correctly deployed JavaFX application. main() serves only as fallback in case the application can not be launched through deployment artifacts, e.g., in IDEs with limited FX support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }

}
