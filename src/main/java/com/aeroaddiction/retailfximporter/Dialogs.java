/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroaddiction.retailfximporter;

import com.aeroaddiction.retailfximporter.controllers.MainController;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Randy
 */
public class Dialogs
{

    /**
     *
     * @param title
     * @param header
     * @param content
     * @param alertType
     */
    private static void showAlert(String title, String header, String content, Alert.AlertType alertType)
    {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.initOwner(RetailFxImporter.getRootStage());
        alert.showAndWait();
    }

    /**
     *
     * @param title
     * @param header
     * @param content
     */
    public static void showError(String title, String header, String content)
    {
        showAlert(title, header, content, Alert.AlertType.ERROR);
    }

    /**
     *
     * @param title
     * @param header
     * @param content
     */
    public static void showWarning(String title, String header, String content)
    {
        showAlert(title, header, content, Alert.AlertType.WARNING);
    }

    /**
     *
     * @param title
     * @param header
     * @param content
     */
    public static void showInformation(String title, String header, String content)
    {
        showAlert(title, header, content, Alert.AlertType.INFORMATION);
    }

    /**
     *
     * @param title
     * @param header
     * @param content
     * @return
     */
    public static Optional<ButtonType> showConfirmation(String title, String header, String content)
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.initOwner(RetailFxImporter.getRootStage());

        return alert.showAndWait();
    }

    public static void showException(String title, Exception ex)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText("Exception: " + ex.getMessage());
        alert.initOwner(RetailFxImporter.getRootStage());

        String exceptionText = "";
        for (StackTraceElement element : ex.getStackTrace()) {
            exceptionText += element + "\n";
        }

        Label label = new Label("The exception stacktrace was:");
        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        Label labelCause = new Label("The exception cause was:");
        String causeText = "";

        if (ex.getCause() != null) {
            alert.setContentText("Cause: " + ex.getCause().getMessage());
            for (StackTraceElement element : ex.getCause().getStackTrace()) {
                causeText += element + "\n";
            }
        }
        TextArea textAreaCause = new TextArea(causeText);
        textAreaCause.setEditable(false);
        textAreaCause.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        textAreaCause.setMaxWidth(Double.MAX_VALUE);
        textAreaCause.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        int i = 0;
        expContent.add(label, 0, i++);
        expContent.add(textArea, 0, i++);
        expContent.add(labelCause, 0, i++);
        expContent.add(textAreaCause, 0, i++);

        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    /**
     *
     * @return
     */
    public static Stage LoadingScreenIndeterminate()
    {
        Stage dialog = new Stage();
        dialog.initOwner(RetailFxImporter.getRootStage());
        dialog.initStyle(StageStyle.TRANSPARENT);
        ProgressIndicator progressIndicator = new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(progressIndicator, 0, 0);
        dialog.setScene(new Scene(expContent));
        return dialog;
    }

    /**
     *
     * @param resource
     * @param title
     * @param modality
     * @param resizable
     * @param closeAll
     */
    public static void showDialog(String resource, String title, Modality modality, boolean resizable, boolean closeAll)
    {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource(resource));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle(title);
            dialogStage.initModality(modality);
            dialogStage.initOwner(RetailFxImporter.getRootStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            if (closeAll) {
                dialogStage.setOnCloseRequest(RetailFxImporter.confirmCloseEventHandler);
            }

            if (resizable) {
                dialogStage.setResizable(true);
            } else {
                dialogStage.setResizable(false);
            }

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();
        } catch (IOException ex) {
            Logger.getLogger(Dialogs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param page
     * @param title
     * @param modality
     * @param resizable
     * @param closeAll
     */
    public static void showDialogModify(AnchorPane page, String title, Modality modality, boolean resizable, boolean closeAll)
    {
        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle(title);
        dialogStage.initModality(modality);
        dialogStage.initOwner(RetailFxImporter.getRootStage());
        Scene scene = new Scene(page);
        dialogStage.setScene(scene);

        if (closeAll) {
            dialogStage.setOnCloseRequest(RetailFxImporter.confirmCloseEventHandler);
        }

        if (resizable) {
            dialogStage.setResizable(true);
        } else {
            dialogStage.setResizable(false);
        }

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();
    }
}
