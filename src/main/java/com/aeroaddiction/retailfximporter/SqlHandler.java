/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroaddiction.retailfximporter;

import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.ini4j.Profile.Section;

/**
 *
 * @author Randy
 */
public class SqlHandler
{
    private final File baseFolder;

    public SqlHandler(File baseFolder)
    {
        this.baseFolder = baseFolder;
    }

    public boolean setupDatabase()
    {
        RetailFxImporter.getMainController().appendOutputMessage("Setting up database");
        return createDatabase(true);
    }

    public boolean restoreDatabase()
    {
        RetailFxImporter.getMainController().appendOutputMessage("Restoring Template Structure");
        ArrayList<File> masterList = new ArrayList<>();

        File tableFolder = new File(baseFolder + "\\tables\\structure");
        ArrayList<File> tableList = new ArrayList<>(Arrays.asList(tableFolder.listFiles()));
        ArrayList<File> sortedTableList = sortTableList(tableList);
        masterList.addAll(sortedTableList);

        File viewFolder = new File(baseFolder + "\\views");
        ArrayList<File> viewList = new ArrayList<>(Arrays.asList(viewFolder.listFiles()));
        masterList.addAll(viewList);

        File routineFolder = new File(baseFolder + "\\routines");
        ArrayList<File> routineList = new ArrayList<>(Arrays.asList(routineFolder.listFiles()));
        masterList.addAll(routineList);

        boolean success = true;
        for (File file : masterList) {
            success &= restoreTemplateStructure(file.getName(), file.getAbsolutePath());
            if (!success) {
                return false;
            }
        }

        File dataFolder = new File(baseFolder + "\\tables\\data");
        ArrayList<File> dataList = new ArrayList<>(Arrays.asList(dataFolder.listFiles()));
        for (File file : dataList) {
            success &= restoreTemplateData(file.getName(), file.getAbsolutePath());
            if (!success) {
                return false;
            }
        }

        return true;
    }

    private ArrayList<File> sortTableList(ArrayList<File> tableList)
    {

        try {
            ArrayList<File> masterList = new ArrayList<>();
            List<String> orderedTables = new ArrayList<>();
            if (Files.exists(Paths.get(RetailFxImporter.ORDERED_TABLES))) {
                orderedTables = Files.readAllLines(Paths.get(RetailFxImporter.ORDERED_TABLES), StandardCharsets.UTF_8);
            } else {
                return tableList;
            }

            for (String orderedTable : orderedTables) {
                for (File dbTable : tableList) {
                    if (orderedTable.equals(FilenameUtils.removeExtension(dbTable.getName()))) {
                        masterList.add(dbTable);
                    }
                }
            }

            return masterList;

        } catch (IOException ex) {
            Logger.getLogger(SqlHandler.class.getName()).log(Level.SEVERE, null, ex);
            RetailFxImporter.getMainController().appendOutputMessage(ex.getMessage());
            Dialogs.showException("Error Creating Database", ex);
            return null;
        }
    }

    private String removeMysqlWarningMessage(String message)
    {
        ArrayList<String> finalList = new ArrayList<>();
        String[] lines = message.split("\n");
        for (String line : lines) {
            if (!line.contains("password on the command line")) {
                finalList.add(line);
            }
        }
        return StringUtils.join(finalList, "\n");
    }

    private boolean createDatabase(boolean drop)
    {
        if (drop) {
            dropDatabase();
        }

        RetailFxImporter.getMainController().appendOutputMessage("Creating database");
        String query = "CREATE DATABASE IF NOT EXISTS " + RetailFxImporter.sqlClass.getDatabase();
        try (
                Connection conn = RetailFxImporter.sqlClass.getDataSourceNoDatabase().getConnection();
                PreparedStatement ps = conn.prepareStatement(query);) {
            ps.executeUpdate();
            RetailFxImporter.sqlClass.getDataSource(true);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SqlHandler.class.getName()).log(Level.SEVERE, null, ex);
            RetailFxImporter.getMainController().appendOutputMessage(ex.getMessage());
            Dialogs.showException("Error Creating Database", ex);
            return false;
        }
    }

    private boolean dropDatabase()
    {
        RetailFxImporter.getMainController().appendOutputMessage("Dropping database");
        String query = "DROP DATABASE IF EXISTS " + RetailFxImporter.sqlClass.getDatabase();

        try (
                Connection conn = RetailFxImporter.sqlClass.getDataSourceNoDatabase().getConnection();
                PreparedStatement ps = conn.prepareStatement(query);) {
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SqlHandler.class.getName()).log(Level.SEVERE, null, ex);
            RetailFxImporter.getMainController().appendOutputMessage(ex.getMessage());
            Dialogs.showException("Error Dropping Database", ex);
            return false;
        }
    }

    private boolean restoreTemplateStructure(String fileName, String path)
    {
        try {
            String[] executeCmd = new String[]{
                "mysql", RetailFxImporter.sqlClass.getDatabase(),
                "-u" + RetailFxImporter.sqlClass.getUsername(),
                "-p" + RetailFxImporter.sqlClass.getPassword(),
                "-h" + RetailFxImporter.sqlClass.getAddress(),
                "-P" + RetailFxImporter.sqlClass.getPort(),
                "-e", " source " + path
            };

            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();

            String inputStream = new BufferedReader(new InputStreamReader(runtimeProcess.getInputStream())).lines().collect(Collectors.joining("\n"));
            String errorStream = new BufferedReader(new InputStreamReader(runtimeProcess.getErrorStream())).lines().collect(Collectors.joining("\n"));

            if (!"".equals(inputStream)) {
                RetailFxImporter.getMainController().appendOutputMessage("input: " + inputStream);
            }
            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0) {
                RetailFxImporter.getMainController().appendOutputMessage("Successfully restored SQL structure from : " + fileName);
                return true;
            } else {
                RetailFxImporter.getMainController().appendOutputMessage("error: " + removeMysqlWarningMessage(errorStream));
            }
        } catch (IOException | InterruptedException | HeadlessException ex) {
            Logger.getLogger(SqlHandler.class.getName()).log(Level.SEVERE, null, ex);
            RetailFxImporter.getMainController().appendOutputMessage(ex.getMessage());
            Dialogs.showException("Error Restoring Template Structure", ex);
        }
        return false;
    }

    private boolean restoreTemplateData(String fileName, String path)
    {
        try {
            String[] executeCmd = new String[]{
                "mysql", RetailFxImporter.sqlClass.getDatabase(),
                "-u" + RetailFxImporter.sqlClass.getUsername(),
                "-p" + RetailFxImporter.sqlClass.getPassword(),
                "-h" + RetailFxImporter.sqlClass.getAddress(),
                "-P" + RetailFxImporter.sqlClass.getPort(),
                "-e", " source " + path
            };

            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();

            String inputStream = new BufferedReader(new InputStreamReader(runtimeProcess.getInputStream())).lines().collect(Collectors.joining("\n"));
            String errorStream = new BufferedReader(new InputStreamReader(runtimeProcess.getErrorStream())).lines().collect(Collectors.joining("\n"));

            if (!"".equals(inputStream)) {
                RetailFxImporter.getMainController().appendOutputMessage("input: " + inputStream);
            }
            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0) {
                RetailFxImporter.getMainController().appendOutputMessage("Successfully restored SQL data from : " + fileName);
                return true;
            } else {
                RetailFxImporter.getMainController().appendOutputMessage("error: " + removeMysqlWarningMessage(errorStream));
            }
        } catch (IOException | InterruptedException | HeadlessException ex) {
            Logger.getLogger(SqlHandler.class.getName()).log(Level.SEVERE, null, ex);
            RetailFxImporter.getMainController().appendOutputMessage(ex.getMessage());
            Dialogs.showException("Error Restoring Template Data", ex);
        }
        return false;
    }

    public boolean grantDaemonPermissions()
    {
        RetailFxImporter.getMainController().appendOutputMessage("Granting daemon permissions");
        try {
            File file = generateCreateGrantFile();
            String[] executeCmd = new String[]{
                "mysql", RetailFxImporter.sqlClass.getDatabase(),
                "-u" + RetailFxImporter.sqlClass.getUsername(),
                "-p" + RetailFxImporter.sqlClass.getPassword(),
                "-h" + RetailFxImporter.sqlClass.getAddress(),
                "-P" + RetailFxImporter.sqlClass.getPort(),
                "-e source " + file.getAbsolutePath()
            };

            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();

            String inputStream = new BufferedReader(new InputStreamReader(runtimeProcess.getInputStream())).lines().collect(Collectors.joining("\n"));
            String errorStream = new BufferedReader(new InputStreamReader(runtimeProcess.getErrorStream())).lines().collect(Collectors.joining("\n"));

            if (!"".equals(inputStream)) {
                RetailFxImporter.getMainController().appendOutputMessage("input: " + inputStream);
            }
            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0) {
                RetailFxImporter.getMainController().appendOutputMessage("Successfully granted user permissions");
                return true;
            } else {
                RetailFxImporter.getMainController().appendOutputMessage("error: " + removeMysqlWarningMessage(errorStream));
            }
        } catch (IOException | InterruptedException | HeadlessException ex) {
            Logger.getLogger(SqlHandler.class.getName()).log(Level.SEVERE, null, ex);
            RetailFxImporter.getMainController().appendOutputMessage(ex.getMessage());
            Dialogs.showException("Error Granting User Permissions", ex);
        }
        return false;
    }

    private File generateCreateGrantFile()
    {
        String path = System.getenv("PROGRAMDATA") + File.separator + "RetailFxImporter" + File.separator + "Grant.sql";
        try {
            Section section = PropertiesManager.getSystemIniSection("SQL");
            String daemonUser = section.get("daemonUser");
            String daemonPassword = section.get("daemonPassword");

            ArrayList<String> fileContents = new ArrayList<>();

            String create = String.format("CREATE USER IF NOT EXISTS '%s'@'%%' IDENTIFIED BY '%s';", daemonUser, daemonPassword);
            fileContents.add(create);
            String grant = String.format("GRANT SELECT,INSERT,UPDATE,DELETE,EXECUTE,SHOW VIEW ON %s.* TO '%s'@'%%'; flush privileges;", RetailFxImporter.sqlClass.getDatabase(), daemonUser);
            fileContents.add(grant);

            Files.write(Paths.get(path), fileContents);
        } catch (IOException ex) {
            Logger.getLogger(SqlHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new File(path);
    }
}
