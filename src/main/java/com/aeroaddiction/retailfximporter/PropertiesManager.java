/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroaddiction.retailfximporter;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ini4j.Ini;
import org.ini4j.Profile;
import org.ini4j.Profile.Section;

/**
 *
 * @author ccccfv
 */
public class PropertiesManager
{

    public static final String sysPropertiesFile = System.getenv("PROGRAMDATA") + File.separator + "RetailFxImporter" + File.separator + "System.ini";
    public static final String usrPropertiesFile = System.getenv("LOCALAPPDATA") + File.separator + "RetailFxImporter" + File.separator + "User.ini";

    /**
     *
     * @return
     */
    public static Ini getUserIni()
    {
        try {
            File userFile = new File(usrPropertiesFile);
            if (!userFile.exists()) {
                userFile.getParentFile().mkdirs();
                userFile.createNewFile();
            }
            Ini ini = new Ini();
            ini.load(userFile);

            return ini;

        } catch (IOException ex) {
            Logger.getLogger(PropertiesManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     *
     * @return
     */
    public static Ini getSystemIni()
    {
        try {
            File sysFile = new File(sysPropertiesFile);
            if (!sysFile.exists()) {
                sysFile.getParentFile().mkdirs();
                sysFile.createNewFile();
            }
            Ini ini = new Ini();
            ini.load(sysFile);

            return ini;

        } catch (IOException ex) {
            Logger.getLogger(PropertiesManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     *
     * @param ini
     * @return
     */
    public static boolean storeUserIni(Ini ini)
    {
        try {
            ini.store(new File(usrPropertiesFile));
            return true;
        } catch (IOException ex) {
            Logger.getLogger(PropertiesManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     *
     * @param ini
     * @return
     */
    public static boolean storeSystemIni(Ini ini)
    {
        try {
            ini.store(new File(sysPropertiesFile));
            return true;
        } catch (IOException ex) {
            Logger.getLogger(PropertiesManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     *
     * @param sectionName
     * @return
     */
    public static Section getUserIniSection(String sectionName)
    {
        Ini ini = getUserIni();
        return ini.get(sectionName);
    }

    /**
     *
     * @param sectionName
     * @return
     */
    public static Section getSystemIniSection(String sectionName)
    {
        Ini ini = getSystemIni();
        return ini.get(sectionName);
    }

    /**
     *
     * @param sectionName
     * @param fieldName
     * @return
     */
    public static String getUserIniValueInSection(String sectionName, String fieldName)
    {
        Profile.Section section = getUserIniSection(sectionName);
        return section.get(fieldName);
    }

    /**
     *
     * @param sectionName
     * @param fieldName
     * @return
     */
    public static String getSystemIniValueInSection(String sectionName, String fieldName)
    {
        Profile.Section section = getSystemIniSection(sectionName);
        return section.get(fieldName);
    }
}
