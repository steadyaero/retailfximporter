/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroaddiction.retailfximporter.controllers;

import com.aeroaddiction.retailfximporter.Dialogs;
import com.aeroaddiction.retailfximporter.RetailFxImporter;
import com.aeroaddiction.retailfximporter.SqlController;
import com.sun.javafx.scene.control.skin.LabeledText;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;

/**
 * FXML Controller class
 *
 * @author Randy
 */
public class Dialog_SqlTableImportOrderController implements Initializable
{

    private SqlController sqlController;

    @FXML
    private AnchorPane anchor;
    @FXML
    private ListView<String> list;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        initController();
        populateTable();
    }

    private void initController()
    {
        sqlController = new SqlController();
    }

    private void populateTable()
    {
        try {
            File tableFolder = new File(RetailFxImporter.getMainController().baseFolder.getAbsoluteFile() + "\\tables\\structure");
            ArrayList<File> dbTables = new ArrayList<>(Arrays.asList(tableFolder.listFiles()));

            List<String> orderedTables = new ArrayList<>();
            if (Files.exists(Paths.get(RetailFxImporter.ORDERED_TABLES))) {
                orderedTables = Files.readAllLines(Paths.get(RetailFxImporter.ORDERED_TABLES), StandardCharsets.UTF_8);
            }

            for (File dbTable : dbTables) {
                String tableName = FilenameUtils.removeExtension(dbTable.getName());
                if (!orderedTables.contains(tableName)) {
                    orderedTables.add(tableName);
                }
            }

            list.getItems().addAll(orderedTables);

        } catch (IOException | NullPointerException ex) {
            Logger.getLogger(Dialog_SqlTableImportOrderController.class.getName()).log(Level.SEVERE, null, ex);
            RetailFxImporter.getMainController().appendOutputMessage(ex.getMessage());
            Dialogs.showException("Error populating table Import Order list", ex);
        }
    }

//    private int getIndex(String item)
//    {
//        return list.getItems().indexOf(item);
//    }
    @FXML
    private void onSave(ActionEvent event)
    {
        try {
            Files.write(Paths.get(RetailFxImporter.ORDERED_TABLES), list.getItems(), StandardCharsets.UTF_8);
            ((Stage) anchor.getScene().getWindow()).close();
        } catch (IOException ex) {
            Logger.getLogger(Dialog_SqlTableImportOrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onDragEntered(DragEvent event)
    {
    }

    @FXML
    private void onDragDetected(MouseEvent event)
    {
        Dragboard db = list.startDragAndDrop(TransferMode.MOVE);
        String item = list.getSelectionModel().getSelectedItem();
        ClipboardContent content = new ClipboardContent();
        if (item != null) {
            content.put(RetailFxImporter.dataFormat, item);
        } else {
            content.put(RetailFxImporter.dataFormat, "XData");
        }
        db.setContent(content);
        event.consume();
    }

    @FXML
    private void onDragExited(DragEvent event)
    {
    }

    @FXML
    private void onDragOver(DragEvent event)
    {
        event.acceptTransferModes(TransferMode.MOVE);
    }

    @FXML
    private void onDragDone(DragEvent event)
    {
    }

    @FXML
    private void onDragDropped(DragEvent event)
    {
        String draggedItem = (String) event.getDragboard().getContent(RetailFxImporter.dataFormat);
        int draggedIndex = list.getItems().indexOf(draggedItem);
        String droppedOnItem = ((LabeledText) event.getPickResult().getIntersectedNode()).getText();
        int droppedOnIndex = list.getItems().indexOf(droppedOnItem);
        list.getItems().remove(draggedIndex);
        list.getItems().add((droppedOnIndex == 0) ? 1 : droppedOnIndex, draggedItem);
    }

}
