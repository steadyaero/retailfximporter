/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeroaddiction.retailfximporter.controllers;

import com.aeroaddiction.retailfximporter.Dialogs;
import com.aeroaddiction.retailfximporter.PropertiesManager;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.ini4j.Ini;
import org.ini4j.Profile.Section;

/**
 * FXML Controller class
 *
 * @author Randy
 */
public class Dialog_SqlCredentialsController implements Initializable
{
    @FXML
    private AnchorPane anchor;
    @FXML
    private TextField txtHostname;
    @FXML
    private TextField txtSchema;
    @FXML
    private Spinner<Integer> spPort;
    @FXML
    private TextField txtUsername;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private TextField txtDaemonUser;
    @FXML
    private PasswordField txtDaemonPassword;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        spPort.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 66000, 3306));
        setParameterValues();
    }

    private void setParameterValues()
    {
        Section section = PropertiesManager.getSystemIniSection("SQL");
        if (section == null) {
            Dialogs.showError("Invalid SQL Parameters", "Specify server settings in the credentials dialog", "");
            return;
        }
        String address = section.get("address");
        if (address == null || address.equals("")) {
            Dialogs.showError("Invalid SQL Address", "Address parameter not provided or is invalid", "Update in the credentials dialog");
            return;
        }
        String user = section.get("username");
        if (user == null || user.equals("")) {
            Dialogs.showError("Invalid SQL User", "User parameter not provided or is invalid", "Update in the credentials dialog");
            return;
        }
        String pass = section.get("password");
        if (pass == null || pass.equals("")) {
            Dialogs.showError("Invalid SQL Password", "Password parameter not provided or is invalid", "Update in the credentials dialog");
            return;
        }
        String database = section.get("database");
        if (database == null || database.equals("")) {
            Dialogs.showError("Invalid SQL Database", "Database parameter not provided or is invalid", "Update in the credentials dialog");
            return;
        }
        String portStr = section.get("port");
        if (portStr == null || portStr.equals("")) {
            Dialogs.showError("Invalid SQL Port", "Port parameter not provided or is invalid", "Update in the credentials dialog");
            return;
        }
        int port = Integer.valueOf(portStr);

        String daemonUser = section.get("daemonUser");
        if (daemonUser == null || daemonUser.equals("")) {
            Dialogs.showError("Invalid SQL Daemon User", "Daemon User parameter not provided or is invalid", "Update in the credentials dialog");
            return;
        }

        String daemonPassword = section.get("daemonPassword");
        if (daemonPassword == null || daemonPassword.equals("")) {
            Dialogs.showError("Invalid SQL Daemon Password", "Daemon Password parameter not provided or is invalid", "Update in the credentials dialogb");
            return;
        }

        txtHostname.setText(address);
        txtSchema.setText(database);
        spPort.getValueFactory().setValue(port);
        txtUsername.setText(user);
        txtPassword.setText(pass);
        txtDaemonUser.setText(daemonUser);
        txtDaemonPassword.setText(daemonPassword);
    }

    @FXML
    private void onConnect(ActionEvent event)
    {
        String hostname = txtHostname.getText();
        String database = txtSchema.getText();
        String username = txtUsername.getText();
        String password = txtPassword.getText();
        String daemonUser = txtDaemonUser.getText();
        String daemonPassword = txtDaemonPassword.getText();
        int port = spPort.getValueFactory().getValue();

        Ini ini = PropertiesManager.getSystemIni();
        Section section = ini.get("SQL");
        section.put("address", hostname);
        section.put("username", username);
        section.put("password", password);
        section.put("database", database);
        section.put("port", port);
        section.put("daemonUser", daemonUser);
        section.put("daemonPassword", daemonPassword);

        PropertiesManager.storeSystemIni(ini);
        ((Stage) anchor.getScene().getWindow()).close();
    }

}
