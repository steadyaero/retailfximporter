package com.aeroaddiction.retailfximporter.controllers;

import com.aeroaddiction.retailfximporter.Dialogs;
import com.aeroaddiction.retailfximporter.PropertiesManager;
import com.aeroaddiction.retailfximporter.RetailFxImporter;
import com.aeroaddiction.retailfximporter.SqlHandler;
import com.aeroaddiction.sqlconnector.SqlClass;
import converters.retailpro.RetailPro;
import java.io.File;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import org.ini4j.Profile;

public class MainController implements Initializable
{

    public File baseFolder;
    public File sourceDataFolder;
    private long startStamp;

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SqlHandler sqlHandler;

    @FXML
    private AnchorPane anchor;
    @FXML
    private TextField txtBaseFolder;
    @FXML
    private TextArea txtOutput;
    @FXML
    private ChoiceBox<String> cbConversionSource;
    @FXML
    private TextField txtSourceDataFolder;

    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        startStamp = System.nanoTime();
        initConversionSources();

        if (RetailFxImporter.debugging) {
            this.baseFolder = new File("C:\\Users\\Randy\\Documents\\Programming\\GitLab\\Java\\ErpSqlLibrary\\database");
            txtBaseFolder.setText(this.baseFolder.getAbsolutePath());

            cbConversionSource.getSelectionModel().select("RetailPro");

            this.sourceDataFolder = new File("C:\\Users\\Randy\\Desktop\\RetailPro\\input");
            txtSourceDataFolder.setText(this.sourceDataFolder.getAbsolutePath());
        }

        Platform.runLater(()
                -> {
            initSql();
            sqlHandler = new SqlHandler(baseFolder);
        });
    }

    private void initSql()
    {
        Profile.Section section = PropertiesManager.getSystemIniSection("SQL");
        if (section == null) {
            Dialogs.showError("Invalid SQL Parameters", "Specify server settings in the database settings dialog from the config tab", "");
//            System.exit(1);
        }
        String address = section.get("address");
        if (address == null || address.equals("")) {
            Dialogs.showError("Invalid SQL Address", "Address parameter not provided or is invalid", "Update in the database settings dialog from the config tab");
//            System.exit(1);
        }
        String user = section.get("username");
        if (user == null || user.equals("")) {
            Dialogs.showError("Invalid SQL User", "User parameter not provided or is invalid", "Update in the database settings dialog from the config tab");
//            System.exit(1);
        }
        String pass = section.get("password");
        if (pass == null || pass.equals("")) {
            Dialogs.showError("Invalid SQL Password", "Password parameter not provided or is invalid", "Update in the database settings dialog from the config tab");
//            System.exit(1);
        }
        String database = section.get("database");
        if (database == null || database.equals("")) {
            Dialogs.showError("Invalid SQL Database", "Database parameter not provided or is invalid", "Update in the database settings dialog from the config tab");
//            System.exit(1);
        }
        String portStr = section.get("port");
        if (portStr == null || portStr.equals("")) {
            Dialogs.showError("Invalid SQL Port", "Port parameter not provided or is invalid", "Update in the database settings dialog from the config tab");
//            System.exit(1);
        }
        int port = Integer.valueOf(portStr);

        appendOutputMessage("Initializing sql class with:"
                            + "\n\tAddress: " + address
                            + "\n\tDatabase: " + database
                            + "\n\tPort: " + port
                            + "\n\tuser: " + user
        );
        RetailFxImporter.sqlClass = new SqlClass(address, user, pass, port, database);
    }

    private void initConversionSources()
    {
        cbConversionSource.getItems().clear();
        cbConversionSource.getItems().add("");
        cbConversionSource.getItems().add("RetailPro");
    }

    public void appendOutputMessage(String message)
    {
        Calendar cal = Calendar.getInstance();
        String timestamp = dateFormat.format(cal.getTime());
        txtOutput.appendText("\n" + timestamp + " -- " + message);
    }

    private void clearOutput()
    {
        txtOutput.setText("");
    }

    private Task<Boolean> setupDatabase(Task callback)
    {
        Task<Boolean> task = new Task<Boolean>()
        {
            @Override
            protected Boolean call() throws Exception
            {
                return sqlHandler.setupDatabase();
            }

            @Override
            protected void succeeded()
            {
                if (getValue()) {
                    if (callback != null) {
                        new Thread(callback).start();
                    }
                } else {
                    appendOutputMessage("Import failed");
                }
            }

            @Override
            protected void failed()
            {
                appendOutputMessage("Import failed");
            }
        };
        return task;
    }

    private Task<Boolean> restoreDatabase(File baseFolder, Task callback)
    {
        Task<Boolean> task = new Task<Boolean>()
        {
            @Override
            protected Boolean call() throws Exception
            {
                return sqlHandler.restoreDatabase();
            }

            @Override
            protected void succeeded()
            {
                if (getValue()) {
                    if (callback != null) {
                        new Thread(callback).start();
                    }
                } else {
                    appendOutputMessage("Import failed");
                }
            }

            @Override
            protected void failed()
            {
                appendOutputMessage("Import failed");
            }
        };
        return task;
    }

    private Task<Boolean> startConversion(Task callback, File sourceDataFolder)
    {
        Task<Boolean> task = new Task<Boolean>()
        {
            @Override
            protected Boolean call() throws Exception
            {
                String conversionSource = cbConversionSource.getValue();
                boolean result;

                switch (conversionSource) {
                    case "RetailPro":
                        RetailPro retailPro = new RetailPro(sourceDataFolder);
                        result = retailPro.convert();
                        break;

                    default:
                        Dialogs.showError("Invalid Conversion Source", "Specify a valid conversion source", "");
                        appendOutputMessage("Invalid Conversion Source");
                        result = false;
                        break;
                }

                return result;
            }

            @Override
            protected void succeeded()
            {
                if (getValue()) {
                    if (callback != null) {
                        new Thread(callback).start();
                    }
                } else {
                    appendOutputMessage("Import failed");
                }
            }

            @Override
            protected void failed()
            {
                appendOutputMessage("Import failed");
            }
        };
        return task;
    }

    private Task<Boolean> setupDaemonUser(Task callback)
    {
        Task<Boolean> task = new Task<Boolean>()
        {
            @Override
            protected Boolean call() throws Exception
            {
                return sqlHandler.grantDaemonPermissions();
            }

            @Override
            protected void succeeded()
            {
                if (getValue()) {
                    if (callback != null) {
                        new Thread(callback).start();
                    }
                } else {
                    appendOutputMessage("Import failed");
                }
            }

            @Override
            protected void failed()
            {
                appendOutputMessage("Import failed");
            }
        };
        return task;
    }

    private Task<Void> finishTask()
    {
        Task<Void> task = new Task<Void>()
        {
            @Override
            protected Void call() throws Exception
            {
                long endStamp = System.nanoTime();
                appendOutputMessage("Import Complete");
                long diff = endStamp - startStamp;
                double seconds = (double) diff / 1000000000.0;

                int hours = (int) (seconds / 3600);
                int minutes = (int) (seconds % 3600) / 60;
                int secondsRemainder = (int) (seconds % 3600) % 60;
                appendOutputMessage((String.format("Duration: %02d:%02d:%02d", hours, minutes, secondsRemainder)));
                return null;
            }
        };
        return task;
    }

    @FXML
    private void onBaseFolder(ActionEvent event)
    {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Open Base Folder");
        if (RetailFxImporter.debugging) {
            directoryChooser.setInitialDirectory(this.baseFolder);
        }
        File directory = directoryChooser.showDialog(RetailFxImporter.getRootStage());
        if (directory != null) {
            this.baseFolder = directory;
            txtBaseFolder.setText(this.baseFolder.getAbsolutePath());
        }
    }

    @FXML
    private void onImport(ActionEvent event)
    {
        clearOutput();
        appendOutputMessage("Starting Import");

        //create tasks in reverse order passing each one to the previous executing task
        Task<Void> finishTask = finishTask();
        Task<Boolean> convertTask = startConversion(finishTask, this.sourceDataFolder);
        Task<Boolean> restoreDatabaseTask = restoreDatabase(this.baseFolder, convertTask);
        Task<Boolean> grantPermissionTask = setupDaemonUser(restoreDatabaseTask);
        Task<Boolean> setupDatabaseTask = setupDatabase(grantPermissionTask);

        new Thread(setupDatabaseTask).start();
    }

    @FXML
    private void onSourceDataFolder(ActionEvent event)
    {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Open Base Folder");
        if (RetailFxImporter.debugging) {
            directoryChooser.setInitialDirectory(this.sourceDataFolder);
        }
        File directory = directoryChooser.showDialog(RetailFxImporter.getRootStage());
        if (directory != null) {
            this.sourceDataFolder = directory;
            txtSourceDataFolder.setText(this.sourceDataFolder.getAbsolutePath());
        }
    }

    @FXML
    private void onSetTableOrder(ActionEvent event)
    {
        Dialogs.showDialog(RetailFxImporter.FXML_PATH + "Dialog_SqlTableImportOrder.fxml", "Table Import Order", Modality.APPLICATION_MODAL, false, false);
    }

    @FXML
    private void onClearOutput(ActionEvent event)
    {
        clearOutput();
    }

    @FXML
    private void onModifyCredentials(ActionEvent event)
    {
        Dialogs.showDialog(RetailFxImporter.FXML_PATH + "Dialog_SqlCredentials.fxml", "Sql Parameters", Modality.WINDOW_MODAL, false, false);
        clearOutput();
        initSql();
    }
}
